<?php
include 'db_connection_data.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>
        Chat example
    </title>
    <link href="style.min.css" rel="stylesheet">
    <script src="script.min.js" async=""></script>
</head>
<body>
    <div class="container">
        <div class="message_container">

        </div>
        <div class="write-message">
            <?php
            include 'script.php';
            if(isset($_SESSION['user-id'])){
                if($dbRow){
                    echo "
                        <div
                            class=\"bottom-container\"
                        >
                            <div class='bottom-container__user-name'>
                                <div>
                                    Вітаю, <b>" . $dbRow[0] . "</b>
                                </div>
                                <div class='flex-space'></div>
                                <button class='js-logout'>
                                    вийти
                                </button>
                            </div>
                            <div class=\"bottom-container__message\">
                                <textarea class=\"js--message\"></textarea>
                            </div>
                            <div class=\"bottom-container__button-container\">
                                <button
                                    class=\"js--call-message bottom-container__button-call\"
                                    title='Ctrl + Enter'
                                >
                                    відправити
                                </button>
                            </div>
                        </div>
                    ";

                } else {
                    echo "
                    <div class=\"write-message__error\">
                        Доступ надано тільки зареєстрованим користувачам
                    </div>
                ";
                    include 'registration/index.php';
                }
            } else {
                echo "
                    <div class=\"write-message__error\">
                        Доступ надано тільки зареєстрованим користувачам
                    </div>
                ";
                include 'registration/index.php';
            }
            ?>
        </div>
    </div>
</body>
</html>
