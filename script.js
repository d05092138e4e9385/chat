let previousUseName = undefined;
let numberLastMessage = 0;
let timeZone;
let ctrlEnter = undefined;

if( document.readyState !== 'loading' ) {
    init();
} else {
    document.addEventListener("DOMContentLoaded", init);
}

function init(){
    if(document.querySelector('.js--call-message')){
        document.querySelector('.js--call-message').addEventListener('click', callMessage);
    }
    if(document.querySelector('.js--message')){
        document.querySelector('.js--message').addEventListener('keydown', quickCallMessage);
        document.querySelector('.js--message').addEventListener('keyup', clearCtrlEnter);
    }
    if(document.querySelector('.js-logout')){
        document.querySelector('.js-logout').addEventListener('click', logout);
    }
    if(document.querySelector('.js-reg-pass-icon')){
        document.querySelector('.js-reg-pass-icon').addEventListener('click', regShowHidePassword);
    }

    clearInputArea();
    setGMT();
    scrollDown();
    showNemMessage();
    setInterval(showNemMessage, 1000);
}

function regShowHidePassword(){
    let elemButton = this;
    let elemInput = this.parentElement.parentElement.querySelector('input');

    if(elemInput.getAttribute('type') === 'password'){
        elemInput.setAttribute('type',  'text');
        elemButton.classList.remove('fa-eye-slash');
        elemButton.classList.add('fa-eye');
    } else {
        elemInput.setAttribute('type',  'password');
        elemButton.classList.remove('fa-eye');
        elemButton.classList.add('fa-eye-slash');
    }
}

function setGMT(){
    //console.log(new Date().getTimezoneOffset() * -1);
    timeZone = new Date().getTimezoneOffset() * -1;
    //console.log(timeZone);
}

function logout(){
    sendXMLHttpRequest('registration/registerUser.php',[['logout', '1']],function(){location.reload();});
}
function clearInputArea(){
    if(document.querySelector('.js--message')){
        document.querySelector('.js--message').value = '';
    }
}
function scrollDown() {
    document.querySelector('.container').scrollIntoView(false);
}

function callMessage(){
    let userId = getCookie('user-id');
    let message = encodeURIComponent(document.querySelector('.js--message').value);

    sendXMLHttpRequest('message.php',[['id', userId], ['message', message]]);
    clearInputArea();
    showNemMessage();
}
function getCookie(name){
    let match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
    if (match) return match[2];
}

function sendXMLHttpRequest(phpFile, arrArrNameValue, answerFunction = undefined){
    let xmlhttp = new XMLHttpRequest();
    let dataSend = arrArrNameValue.map(arr => arr[0] + '=' + arr[1] + '&');
    dataSend = dataSend.join('');

    xmlhttp.open("POST", phpFile, true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200){
            if(answerFunction !== undefined){
                answerFunction(this.responseText); // echo from php
            }
        }
    };
    xmlhttp.send(dataSend);
}

function quickCallMessage(e){
    if(
        (e.code === 'ControlLeft' || e.code === 'ControlRight') ||
        (e.code === 'Enter' || e.code === 'NumpadEnter')
    ){
        if(
            (e.code === 'ControlLeft' || e.code === 'ControlRight') &&
            (ctrlEnter === 'Enter' || ctrlEnter === 'NumpadEnter') ||
            (e.code === 'Enter' || e.code === 'NumpadEnter') &&
            (ctrlEnter === 'ControlLeft' || ctrlEnter === 'ControlRight')
        ){
            callMessage();
            ctrlEnter = undefined;
        } else {
            ctrlEnter = e.code;
        }
    }
}
function clearCtrlEnter(e) {
    if(
        (e.code === 'ControlLeft' || e.code === 'ControlRight') ||
        (e.code === 'Enter' || e.code === 'NumpadEnter')){
        if(ctrlEnter === e.code){
            ctrlEnter = undefined;
        }
    }
}

function writeConsole(str){
    console.log(str);
}
function showNemMessage() {
    sendXMLHttpRequest('message.php', [['numberLastMessage', numberLastMessage]], callFunctionCreateMessage);
}
function callFunctionCreateMessage(arr){
    if(!arr){
        return;
    } else if(arr === '[]'){
        return;
    }

    arr = JSON.parse(arr);
    numberLastMessage = arr[arr.length -1]['id'];
    arr.forEach(q => createMessage(q['name'], q['message'], q['date']));
}
function createMessage(userName, message, date){
    let elem = document.createElement("div");
    let elemInnerHTML;
    let previousMessageStyle = '';

    if(previousUseName !== userName){
        previousMessageStyle = 'message--new-user';
    }
    elem.className = 'message ' + previousMessageStyle;
    previousUseName = userName;

    message = escapeHtml(message);
    message = message.replace( /((http:|https:)+[^\s]+[\w];?)/g, "<a href=\"$1\" target=\"_blank\">$1</a>");
    //message = message.replace( /(^|\s)((www)+[^\s]+[\w];?)/g, "$1<a href=\"http://$2\" target=\"_blank\">$2</a>");
    message = message.replace( /(\n\r)|\n|\r/g, "<br> " );


    date = returnDateForUserTimeZone(date);
    elemInnerHTML =
        `<div class="message__header">
            <div class="message__user-name">` +
                userName +
            `</div>
            <div class="message__data">` +
                date +
            `</div>
        </div>
        <div class="message__text">` +
            message +
        `</div>`
    ;
    elem.innerHTML = elemInnerHTML;
    elem.dataset.userName = userName;

    document.querySelector('.message_container').append(elem);
    scrollDown();
}

function escapeHtml (string) {
    let entityMap = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#39;',
        '/': '&#x2F;',
        '`': '&#x60;',
        '=': '&#x3D;'
    };
    return String(string).replace(/[&<>"'`=\/]/g, function (s) {
        return entityMap[s];
    });
}

function returnDateForUserTimeZone(dateStr){
    let date = new Date(dateStr);
    date.setMinutes(date.getMinutes() + timeZone );

    return date.getFullYear() + '.' + addZeroToOneNumber(date.getMonth() +1) + '.' + addZeroToOneNumber(date.getDate()) + ' ' +
        addZeroToOneNumber(date.getHours()) + ':' + addZeroToOneNumber(date.getMinutes()) + '.' + addZeroToOneNumber(date.getSeconds());
}

function addZeroToOneNumber(number){
    number = number + '';
    return number.length === 1 ? '0' + number : number;
}