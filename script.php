<?php
$dbRow = '';
if(!isset($_SESSION)){
    session_start();
}

if(isset($_SESSION['user-id'])){
    $conn = mysqli_connect(HOST, USER, PASSWORD, DATABASE);
    $conn->set_charset('utf8mb4');

    $dbRow = returnColumnFromDBByData('userID', $_SESSION['user-id']);
    if(!$dbRow) {
        //echo 'Error: user not found'."\n[".$_SESSION['user-id']."]";
        return false;
    }
    setcookie('user-id', $_SESSION['user-id'], time() + 31536000, '/');
    //if(!isset($_COOKIE['user-id'])){
    //    setcookie('user-id', $_SESSION['user-id'], time() + 31536000);
    //}
} else if(isset($_COOKIE['user-id'])){
    $dbRow = returnColumnFromDBByData('userID', $_COOKIE['user-id']);
    if(!$dbRow) {
        return false;
    }
    $_SESSION['user-id'] = $_COOKIE['user-id'];
}

function returnColumnFromDBByData($dataName, $dataValue){
    $conn = new mysqli(HOST, USER, PASSWORD, DATABASE);
    if (!$conn) {
        return false;
    }
    $conn->set_charset('utf8mb4');

    $stmt = $conn -> prepare("SELECT * FROM `chat_user` WHERE ".$dataName." = ?");
    $stmt -> bind_param("s", $dataValue);
    $stmt -> execute();

    $result = $stmt -> get_result();
    $stmt -> fetch();
    $result = $result->fetch_row();

    return $result;
}