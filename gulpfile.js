﻿'use strict';

let gulp = require('gulp'),
    less = require('gulp-less'),
    mediaQuerieCSS = require('gulp-group-css-media-queries'),
    cleanCSS = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    jsMin = require('gulp-uglify');

// CSS
gulp.task('style', () => {
    return gulp.src('./*.less')
        .pipe(less())
        .pipe(mediaQuerieCSS())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./'));
});
gulp.task('style:watch', function () {
    gulp.watch('./*.less', { delay: 2000 }, gulp.series(['style']));
});

// JS
gulp.task('compressed-js', function () {
    return gulp.src(['./*.js', '!./*.min.js', '!gulpfile.js'])
        //.pipe(jsMin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./'));
});
gulp.task('compressed-js:watch', function () {
    gulp.watch('*.js', ['compressed-js']);
    gulp.watch(['./*.js', '!./*.min.js', '!gulpfile.js'], { delay: 2000 }, gulp.series(['compressed-js']));
});