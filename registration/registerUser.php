<?php
include '../db_connection_data.php';

if(isset($_POST['logout'])){
    logout();
    echo 'logout';
} else if (isset($_POST['name']) && isset($_POST['password'])) {
    $name = $_POST['name'];
    $pass = $_POST['password'];

    if (isset($_POST['UserRegistration']))
        registrationUser($name, password_hash($pass, PASSWORD_BCRYPT));
    else if (isset($_POST['UserLogin'])) {
        //echo "pass: " . $pass . "\n";
        loginUser($name, $pass);
    } else {
        //die('unknown post question');
    }
}

function logout(){
    if(!isset($_SESSION)){
        session_start();
    }
    if(isset($_SESSION['user-id'])){
        unset($_SESSION['user-id']);
    }
    setcookie ("user-id", "", time() - 3600, "/");
}
function setSession($name, $pass){
    //if(!isset($_SESSION)){
    //    session_start();
    //}

    $userID = returnNewUserId($name, $pass);

    $_SESSION['user-id'] = $userID;
    setcookie('user-id', $userID, time() + 31536000, '/');

    updateUserIdInDB($name, $userID);
}
function returnNewUserId($name, $pass){
    $randomBytes = bin2hex(random_bytes(10));
    $token = password_hash(($name.$pass.$randomBytes),  PASSWORD_BCRYPT);
    return $token;
}
function updateUserIdInDB($name, $userId){
    $conn = new mysqli(HOST, USER, PASSWORD, DATABASE);
    if (!$conn) {
        return [false, "Connection failed: " . mysqli_connect_error()];
    }
    $conn->set_charset('utf8mb4');

    if (!checkPresenceUserNameInDB($conn, $name)) {
        mysqli_close($conn);
        return [false, "Error: this user name not found\n"];
    }

    $stmt = $conn -> prepare("UPDATE `chat_user` SET userID = ?  WHERE name = ?");
    $stmt -> bind_param("ss", $userId, $name);

    if ($stmt -> execute()) {
        $output = [true, "user id update\n"];
    } else {
        $output = [false, "Error: " . mysqli_error($conn) . "\n"];
    }
    return $output;
}

function registrationUser($name, $pass){
    if(!isset($_SESSION)){
        session_start();
    }
    $name = trim($name);
    $name = htmlspecialchars($name);
    $isUserNameAvailable = insertToDB($name, $pass);
    if($isUserNameAvailable[0]){
        setSession($name, $pass);
        if(isset( $_SESSION['reg-log-error'])){
            unset ($_SESSION['reg-log-error']);
        }
    } else {
        $_SESSION['reg-log-error'] = 'reg';
    }
    header('Location: ../');
}

function insertToDB($name, $pass){
    $conn = new mysqli(HOST, USER, PASSWORD, DATABASE);
    if (!$conn) {
        return [false, "Connection failed: " . mysqli_connect_error()];
    }
    $conn->set_charset('utf8mb4');

    if (checkPresenceUserNameInDB($conn, $name)) {
        return [false, "Error: this user name is already in use\n"];
    }

    $userID = returnNewUserId($name, $pass);

    $stmt = $conn -> prepare("INSERT INTO `chat_user` VALUES (?, ?, ?)");
    $stmt -> bind_param("sss", $name, $pass, $userID);

    if ($stmt -> execute()) {
        $output = [true, "New record created successfully\n"];
    } else {
        $output = [false, "Error: ". mysqli_error($conn) . "\n"];
    }
    return $output;
}

function checkPresenceUserNameInDB($connect, $userName){
    $stmt = $connect -> prepare("SELECT * FROM `chat_user` WHERE name = ?");
    $stmt -> bind_param("s", $userName);

    if (!$stmt -> execute()) {
        echo 'error';
        return false;
    }
    var_dump($stmt);
    $result = $stmt -> get_result();
    var_dump($result);
    $stmt -> fetch();
    if ($result->num_rows > 0) {
        return true;
    }
    return false;
}

function loginUser($name, $pass){
    if(!isset($_SESSION)){
        session_start();
    }
    $name = htmlspecialchars($name);
    $isReg = selectFromDB($name, $pass);

    if($isReg[0]){
        setSession($name, $pass);
        if(isset( $_SESSION['reg-log-error'])){
            unset ($_SESSION['reg-log-error']);
        }
    } else {
        $_SESSION['reg-log-error'] = $isReg[1];
    }
    header('Location: ../');
}

function selectFromDB($name, $pass){
    $conn = new mysqli(HOST, USER, PASSWORD, DATABASE);
    if (!$conn) {
        return [false, "Connection failed: " . mysqli_connect_error()];
    }
    $conn->set_charset('utf8mb4');

    $stmt = $conn -> prepare("SELECT * FROM `chat_user` WHERE name = ?");
    $stmt -> bind_param("s", $name);
    if (!$stmt -> execute()) {
        return [false, 'error' . mysqli_error($conn)];
    }

    $result = $stmt -> get_result();
    $stmt -> fetch();
    if ($result->num_rows < 1) {
        return [false, 'name'];
    } else if ($result->num_rows > 1) {
        return [false, 'Error: double data line in DB'];
    }

    $arr = $result->fetch_row();
    if (password_verify($pass, $arr[1])) {
        $output = [true];
    } else {
        $output = [false, 'pass'];
    }

    return $output;
}
