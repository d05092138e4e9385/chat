<?php
    if(!isset($_SESSION)){
        session_start();
    }
?>
<div class="content">
    <form
        action="registration/registerUser.php"
        method="post"
        class="bottom-container"
    >
        <div class="bottom-container__name">
            <input
                required
                name="name"
                autocomplete="off"
                type="text"
                placeholder="ім'я"
            >
            <div class='bottom-container__error'> 
                <?php
                    if(isset($_SESSION['reg-log-error'])){
                        if($_SESSION['reg-log-error'] == 'reg'){
                            echo "Введене ім'я вже заняте";
                        } else if($_SESSION['reg-log-error'] == 'name'){
                            echo "Неправильне ім'я користувача";
                        }
                    }
                ?>
            </div>
        </div>
        <div class="bottom-container__pass">
            <div class="bottom-container__pass-container">
                <input
                    required
                    name="password"
                    autocomplete="off"
                    type="password"
                    placeholder="пароль"
                >
                <div class="bottom-container__icon-pass">
                    <div class="far fa-eye-slash js-reg-pass-icon"></div>
                </div>
            </div>
            <div class='bottom-container__error'> 
            <?php
            if(isset($_SESSION['reg-log-error'])){
                if($_SESSION['reg-log-error'] == 'pass'){
                    echo "Неправильний пароль";
                }
            }
            ?>
            </div>
        </div>
        <div class="bottom-container__button-container">
            <button
                class="bottom-container__button"
                type="submit"
                name="UserRegistration"
            >
                зареєструватися
            </button>
            <button
                class="bottom-container__button"
                type="submit"
                name="UserLogin"
            >
                увійти
            </button>
        </div>
    </form>
</div>