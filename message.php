<?php
include 'db_connection_data.php';

if(isset($_POST['id']) && isset($_POST['message'])){
    $name = returnUserName($_POST['id']);
    if(!$name){
        return false;
    }
    insertMessage($name, ($_POST['message']));
    //print_r(($_POST['message']));
}
if(isset($_POST['numberLastMessage'])){
    $data = loadMessages($_POST['numberLastMessage']);
    //print_r($data);
    echo json_encode($data);
}

function loadMessages($numberLastMessage){
    $conn = new mysqli(HOST, USER, PASSWORD, DATABASE);
    if (!$conn) {
        return false;
    }
    $conn->set_charset('utf8mb4');

    $stmt = $conn -> prepare("SELECT * FROM `chat_message` WHERE id > ?");
    $stmt -> bind_param("s", $numberLastMessage);
    $stmt -> execute();

    $result = $stmt -> get_result();
    $stmt -> fetch();

    $result = $result->fetch_all( MYSQLI_ASSOC);
    //foreach ($result as &$res){
    //    $res['date'] = date('Y-m-d H:i:s', strtotime($res['date']));
    //}


    return $result;
}

function returnUserName($userId){
    $mysqli = new mysqli(HOST,USER,PASSWORD,DATABASE);
    if (!$mysqli) {
        return false;
    }
    $mysqli->set_charset('utf8mb4');

    $stmt = $mysqli -> prepare("SELECT name FROM `chat_user` WHERE `userID` = ?");
    $stmt -> bind_param("s", $userId);
    $stmt -> execute();

    $stmt -> bind_result($district);
    $stmt -> fetch();

    return $district;
}

function insertMessage($name, $message){
    $message = trim($message);
    if($message == ''){
        return;
    }
    $mysqli = new mysqli(HOST,USER,PASSWORD,DATABASE);
    if ($mysqli -> connect_errno) {
        exit();
    }
    $mysqli->set_charset('utf8mb4');

    $date = gmdate("Y-m-d H:i:s");

    $stmt = $mysqli -> prepare("INSERT INTO `chat_message` (name, message, date) VALUES (?, ?, ?)");
    $stmt -> bind_param("sss", $name, $message, $date);
    $stmt -> fetch();

    $stmt -> execute();

    /*
     $conn = new PDO("mysql:host=".HOST.";dbname=".DATABASE, USER, PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn -> prepare("INSERT INTO message (name, message) VALUES (?, ?)");
    $stmt->bindParam(1, $name);
    $stmt->bindParam(2, $message);

    $stmt -> execute();
    $conn = null;
     */
}

